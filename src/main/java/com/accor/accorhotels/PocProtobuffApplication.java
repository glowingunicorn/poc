package com.accor.accorhotels;

import com.accor.accorhotels.repository.CourseRepository;
import com.accor.api.protobuf.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;

import java.util.*;

@SpringBootApplication
public class PocProtobuffApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocProtobuffApplication.class, args);
    }

    @Bean
    ProtobufHttpMessageConverter protobufHttpMessageConverter() {
        return new ProtobufHttpMessageConverter();
    }

    @Bean
    public CourseRepository createTestCourses() {
        Map<Integer, PersonService.Course> courses = new HashMap<>();
        PersonService.Course course1 = PersonService.Course.newBuilder().setId(1).setCourseName("REST with Spring").addAllStudent(createTestStudents()).build();
        PersonService.Course course2 = PersonService.Course.newBuilder().setId(2).setCourseName("Learn Spring Security").addAllStudent(new ArrayList<>()).build();
        courses.put(course1.getId(), course1);
        courses.put(course2.getId(), course2);
        return new CourseRepository(courses);
    }

    private List<PersonService.Student> createTestStudents() {
        PersonService.Student.PhoneNumber phone1 = createPhone("123456", PersonService.Student.PhoneType.MOBILE);
        PersonService.Student student1 = createStudent(1, "John", "Doe", "john.doe@baeldung.com", Arrays.asList(phone1));
        PersonService.Student.PhoneNumber phone2 = createPhone("234567", PersonService.Student.PhoneType.LANDLINE);
        PersonService.Student student2 = createStudent(2, "Richard", "Roe", "richard.roe@baeldung.com", Arrays.asList(phone2));
        PersonService.Student.PhoneNumber phone3_1 = createPhone("345678", PersonService.Student.PhoneType.MOBILE);
        PersonService.Student.PhoneNumber phone3_2 = createPhone("456789", PersonService.Student.PhoneType.LANDLINE);
        PersonService.Student student3 = createStudent(3, "Jane", "Doe", "jane.doe@baeldung.com", Arrays.asList(phone3_1, phone3_2));
        return Arrays.asList(student1, student2, student3);
    }

    private PersonService.Student createStudent(int id, String firstName, String lastName, String email, List<PersonService.Student.PhoneNumber> phones) {
        return PersonService.Student.newBuilder().setId(id).setFirstName(firstName).setLastName(lastName).setEmail(email).addAllPhone(phones).build();
    }

    private PersonService.Student.PhoneNumber createPhone(String number, PersonService.Student.PhoneType type) {
        return PersonService.Student.PhoneNumber.newBuilder().setNumber(number).setType(type).build();
    }

}
