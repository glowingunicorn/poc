package com.accor.accorhotels.repository;

import java.util.Map;

import com.accor.api.protobuf.PersonService;

public class CourseRepository {

    private final Map<Integer, PersonService.Course> courses;

    public CourseRepository(Map<Integer, PersonService.Course> courses) {
        this.courses = courses;
    }

    public PersonService.Course getCourse(int id) {
        return courses.get(id);
    }
}
