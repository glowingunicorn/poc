package com.accor.accorhotels.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accor.accorhotels.repository.CourseRepository;
import com.accor.api.protobuf.PersonService;

@RestController
public class PersonController {

    @Autowired
    CourseRepository courseRepository;

    @RequestMapping("/person/{id}")
    PersonService.Course customer(@PathVariable Integer id) {
        return courseRepository.getCourse(id);
    }
}
